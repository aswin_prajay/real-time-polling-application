import { configureStore } from '@reduxjs/toolkit';
import authReducer from "./actions/action";

export default configureStore({
    reducer: {
        login: authReducer
    }
})