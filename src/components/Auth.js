import { useEffect, useState, useRef } from "react";
import LoginIcon from '@mui/icons-material/Login';
import { useSelector, useDispatch } from 'react-redux'
import Cookies from 'universal-cookie';

import { authorization, isLoggedIn } from "../actions/action";

const { socket } = require('../socketConnection');

function Auth({children}) {
    const user = useSelector(state => state.login.user);
    const isLoggedStatus = useSelector(state => state.login.loggedIn);
    const cookies = new Cookies();
    const userRef = useRef(null);
    const [users, setUsers] = useState([])

    const dispatch = useDispatch()

    const handleLogin = (val) => {
        dispatch(authorization(val))
        dispatch(isLoggedIn(true))
        cookies.set('name', val, { expires: 0 });
    }
    
    const handleUsers = () => {
        userRef.current.style.display = "flex"
        socket.emit('fetchuser')
    }

    const handleClose = () => {
        userRef.current.style.display = "none"
    }

    const selectUser = (val) => {
        handleLogin(val)
        handleClose()
    }

    useEffect(()=>{
        socket.on('userData', data => {
            setUsers(data)
        })
    }, [])

    useEffect(()=> {
        if (!user) {
            let loggedInUser = cookies.get('name')
            if (!loggedInUser) {
                dispatch(isLoggedIn(false))
            } else {
                dispatch(authorization(loggedInUser))
                dispatch(isLoggedIn(true))
            }
        } 
    }, [user])

    return (
        <div className="auth-main">
            {
                !isLoggedStatus ? (
                    <div className="auth-board">
                        <p className="auth-header text">Login</p>
                        <div className="auth-action">
                            <div onClick={()=>handleLogin('admin')} className="login-action">
                                <p className="text">Admin*</p>
                                <LoginIcon className="text"/>
                            </div>
                            <div onClick={()=>handleUsers()} className="login-action">
                                <p className="text">User**</p>
                                <LoginIcon className="text"/>
                            </div>
                        </div>
                        <div className="auth-desc">
                            <p className="text">*Login as admin to host poll</p>
                            <p className="text">**Login as user to vote poll</p>
                        </div>
                    </div>
                ) : (
                    children
                )
            }
            <div ref={userRef} id="myModal" className="modal">
                <div className="modal-content">
                    <span className="close" onClick={handleClose}>&times;</span>
                    <p>Logged in as</p>
                    <ul>
                        {
                            users.map(userData=>{
                                return <li onClick={()=>selectUser(userData.name)} key={userData.id}>
                                    {userData.name}
                                    <LoginIcon />
                                </li>
                            })
                        }
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Auth