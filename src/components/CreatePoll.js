import { Button } from '@mui/base';

import PollView from './PollView';

const { socket } = require('../socketConnection');

function CreatePoll(props) {
    const {
        configurePoll,
        createPoll,
        handlePollValues,
        poll,
        user,
        addNewOption,
        hostPoll,
        handleOption
    } = props

    const lockPoll = () => {
        socket.emit('lock-poll', poll.id)
    }
    
    return (
        <div className='create-poll'>
            {
                configurePoll ? (
                    <div className='poll-configuration'>
                        <h1>Create Poll</h1>
                        <div className='poll-section'>
                            <input 
                                type="text" 
                                id="poll-question" 
                                placeholder="Enter your question" 
                                onChange={handlePollValues}
                                value={poll.question}
                                autoComplete={false}
                            />
                            <ul>
                                {
                                    poll.options.map(opt=>(
                                        <li key={opt.id}>
                                            <input  type="text" value={opt.option} onChange={(e)=>handleOption(opt.id, e.target.value)} className="option-input" placeholder={`Option ${opt.id}` }/>
                                        </li>
                                    ))
                                }
                                <Button disabled={poll.options.length>=5} className='button' onClick={addNewOption}>Add Option</Button>
                            </ul>
                        </div>
                        <Button className='button' onClick={hostPoll}>Host Poll</Button>
                    </div>
                ) : poll.id ? ( 
                    <PollView lockPoll={lockPoll} user={user} poll={poll} />
                ) : (
                    <div className='new-board'>
                        <p>No poll created, Please create a poll</p>
                        <Button className='button' onClick={createPoll}>Add Poll</Button>
                    </div>
                )
            }
        </div>
    )
}

export default CreatePoll