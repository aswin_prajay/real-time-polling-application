import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Cookies from 'universal-cookie';
import LogoutIcon from '@mui/icons-material/Logout';
import { Button } from '@mui/base';
import HistoryIcon from '@mui/icons-material/History';

import PollView from './PollView';
import { updatePoll, isLoggedIn, authorization } from "../actions/action";
import CreatePoll from './CreatePoll';

const { socket } = require('../socketConnection');

function PollDashBoard() {
    const user = useSelector(state => state.login.user);
    const poll = useSelector(state => state.login.poll);
    const dispatch = useDispatch();
    const cookies = new Cookies();
    const [configurePoll, setConfigurePoll] = useState(false);
    const [history, setHistory] = useState([]);

    const createPoll = () => {
        setConfigurePoll(true)
    }

    const handlePollValues = (e) => {
        let updatedPoll = {
            ...poll,
            question: e.target.value
        }
        dispatch(updatePoll(updatedPoll))
    }

    const handleOption = (id, value) => {
        let newOptions = poll.options.map(e=>{
            if (e.id===id) {
                return {
                    ...e,
                    option: value
                }
            }
            return e
        })
        let updatedPoll = {
            ...poll,
            options: newOptions
        }
        dispatch(updatePoll(updatedPoll))
    }

    const addNewOption = () => {
        if (poll.options.length>5) {
            return
        }
        let updatedPoll = {
            ...poll,
            options: [...poll.options, {id: poll.options.length+1, option: ''}]
        }
        dispatch(updatePoll(updatedPoll))
    }

    const hostPoll = () => {
        socket.emit('createPoll', { poll });
    }

    const handlePoll = (id) => {
        let updatedOptions = poll.options.map(dat=>{
            const opt = {...dat}
            if (opt.id===id) {
                // &&e.users&&!e.users.include(id)
                if (opt.users&&!opt.users.includes(user)) {
                    opt.votes += 1
                    opt.users = [...(opt.users||[]), user]
                } else {
                    opt.users = [user]
                    opt['votes'] = 1
                }
            } else if (opt.users&&opt.users.includes(user)) {
                opt.users = opt.users.filter(e=>e!==user)
                opt['votes'] -= 1
            }
            return opt
        })
        let updatedPoll = {
            ...poll,
            options: updatedOptions
        }
        dispatch(updatePoll(updatedPoll))
        socket.emit('updatepoll', updatedPoll)
    }

    const lockPoll = () => {
        socket.emit('lock-poll', poll.id)
    }

    const handleLogout = () => {
        dispatch(authorization(''))
        dispatch(isLoggedIn(false))
        cookies.remove('name');
    }

    useEffect(()=>{
        socket.emit('activePoll')
        socket.emit('history')
        socket.on('pollCreated', data => {
            let updatedPoll = {
                id: data.pollId,
                question: data.question,
                options: data.options.map(option => ({ ...option, votes: 0 }))
            }
            dispatch(updatePoll(updatedPoll))
            setConfigurePoll(false)
        });
        socket.on('fetchPoll', data => {
            let updatedPoll = {
                id: data.pollId||'',
                question: data.question||'',
                options: data.options||[{id: 1, option: ''}, {id: 2, option: ''}]
            }
            dispatch(updatePoll(updatedPoll))
            if (data.pollId) {
                setConfigurePoll(false)
            }
        });
        socket.on('fetchHistory', data => {
            setHistory(data)
        });
    }, [])

    useEffect(()=> {
        if (!user) {
            let loggedInUser = cookies.get('name')
            if (!loggedInUser) {
                dispatch(isLoggedIn(false))
            } else {
                dispatch(authorization(loggedInUser))
                dispatch(isLoggedIn(true))
            }
        } 
    }, [user])

    return (
        <div className="poll-dashboard">
            <div className='header'>
                <Button onClick={handleLogout} className='button'>
                    <span>Sign Out</span>
                    <LogoutIcon />
                </Button>
            </div>
            <div className='poll-view'>
                {
                    user==='admin'?
                        <CreatePoll 
                            configurePoll={configurePoll}
                            createPoll={createPoll}
                            handlePollValues={handlePollValues}
                            poll={poll}
                            user={user}
                            addNewOption={addNewOption}
                            hostPoll={hostPoll}
                            handleOption={handleOption}
                        />
                    : 
                        <PollView 
                            handlePoll={handlePoll}
                            user={user}
                            poll={poll}
                            lockPoll={lockPoll}
                        />
                }
            </div>
            <div className='poll-history'>
                <p>
                    <HistoryIcon />
                    <span>History</span>
                </p>
                {
                    history.map(data=>(
                        <PollView 
                            style={{
                                height: 'auto',
                                fontSize: '18px',
                                minHeight: '10%',
                                marginTop: '10px',
                                marginBottom: '10px',
                                color: "#000",
                                marginTopsvg: 0,
                                optionHeight: "15px",
                                display: "none"
                            }}
                            poll={data}
                            handlePoll={()=>{}}
                            lockPoll={()=>{}}
                            user={user}
                            key={data.id}
                            history={true}
                        />
                    ))
                }
            </div>
        </div>
    )
}

export default PollDashBoard