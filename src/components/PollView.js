import { Button } from '@mui/base';
import QuestionMarkIcon from '@mui/icons-material/QuestionMark';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import AdjustIcon from '@mui/icons-material/Adjust';

const INTERNAL_USERS = 5; //users are not configurable

function PollView(props) {
    const {
        handlePoll,
        user,
        poll,
        lockPoll,
        style,
        history
    } = props

    return (
        <div style={style?{height: style.height}:!poll.id?{justifyContent: "center", alignItems: "center"}:{}} className='pollview'>
            {
                !poll.id ? (
                    <div className='new-board'>
                        <p>No poll is hosted, Please try later</p>
                    </div>
                ) : (
                    <>
                        <p style={style?{fontSize: style.fontSize, minHeight: style.minHeight, marginTop: style.marginTop, marginBottom: style.marginBottom, color: style.color}:{}}>
                            <QuestionMarkIcon style={style?{marginTop: style.marginTopsvg}:{}} />
                            <span>{poll.question}</span>
                        </p>
                        <ul>
                            {
                                poll.options.map(opt=>(
                                    <li
                                        onClick={()=>user!=='admin'&&handlePoll(opt.id)}
                                        key={opt.id}
                                    >
                                        <div style={user==='admin'||history?{background: `linear-gradient(to right${opt.votes===0?'':`, rgba(0 ,0 ,0 ,0.2) ${(opt.votes/INTERNAL_USERS)*100}%`}, rgba(255,255,255) 0%)`, ...(style?{height: style.optionHeight}:{})}:{}}>
                                            {
                                                history?<AdjustIcon className='checkicon'/>:user==='admin'?<AdjustIcon className='checkicon'/>:opt.users&&opt.users.includes(user) ? <CheckCircleIcon className='checkicon selcted-check'/>: <AdjustIcon className='checkicon'/>
                                            }
                                            <p>{opt.option}</p>

                                        </div>
                                        {user==='admin'&&<p className='votting-percentage'>{((opt.votes/INTERNAL_USERS)*100)+"%"}</p>}
                                    </li>
                                ))
                            }
                            {user==='admin'&&<Button style={style?{display: style.display}:{}} onClick={lockPoll} className='button lock-poll'>Lock Poll</Button>}
                        </ul>
                    </>
                )
            }
        </div>
    )
}

export default PollView