import React from 'react';
import { ErrorBoundary } from 'react-error-boundary'

import './App.css';
import Auth from './components/Auth';
import PollDashBoard from './components/PollDashBoard';

function App() {

  return (
      <ErrorBoundary FallbackComponent={<div>oops</div>}>
        <div className="App">
          <Auth>
            <PollDashBoard />
          </Auth>
      </div>
    </ErrorBoundary>
  );
}

export default App;