import { createSlice } from '@reduxjs/toolkit'

export const authReducer = createSlice({
  name: 'auth',
  initialState: {
    user: '',
    loggedIn: false,
    poll: {
      id: '',
      question: '',
      options: [{id: 1, option: ''}, {id: 2, option: ''}]
    }
  },
  reducers: {
    authorization: (state, action) => {
      state.user = action.payload
    },
    isLoggedIn: (state, action) => {
        state.loggedIn = action.payload
    },
    updatePoll: (state, action) => {
      state.poll = action.payload
    }
  }
})

export const { authorization, isLoggedIn, updatePoll } = authReducer.actions

export default authReducer.reducer