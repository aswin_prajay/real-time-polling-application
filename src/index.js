import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import { ErrorBoundary } from 'react-error-boundary'


import './index.css';
import App from './App';
import store from './reducstore'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <ErrorBoundary FallbackComponent={<div>oops</div>}>
      <Provider store={store}>
          <App />
      </Provider>
    </ErrorBoundary>
);

